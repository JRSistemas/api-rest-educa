package com.jr.sistemas.msv.curso.service;

import com.jr.sistemas.msv.curso.model.Curso;
import java.util.List;
import java.util.Optional;

public interface CursoService {

    Curso save(Curso curso);
    Optional<Curso> findById(Long id);
    List<Curso> findAll();
    void deleteById(Long id);

}
