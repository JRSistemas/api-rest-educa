package com.jr.sistemas.msv.curso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsvCursoApplication {

	public static void main(String[] args) {

		SpringApplication.run(MsvCursoApplication.class, args);
	}

}
