package com.jr.sistemas.msv.curso.service.impl;


import com.jr.sistemas.msv.curso.model.Curso;
import com.jr.sistemas.msv.curso.repositories.CursoRepository;
import com.jr.sistemas.msv.curso.service.CursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class CursoServiceImpl implements CursoService {

    @Autowired
    private CursoRepository cursoRepository;
    @Override
    @Transactional
    public Curso save(Curso curso) {
        return cursoRepository.save(curso);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Curso> findById(Long id)
    {
        return cursoRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Curso> findAll()
    {
        return (List<Curso>)cursoRepository.findAll();
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        cursoRepository.deleteById(id);
    }
}