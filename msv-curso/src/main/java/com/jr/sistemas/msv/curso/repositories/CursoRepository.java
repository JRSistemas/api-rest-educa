package com.jr.sistemas.msv.curso.repositories;

import com.jr.sistemas.msv.curso.model.Curso;
import org.springframework.data.repository.CrudRepository;

public interface CursoRepository extends CrudRepository<Curso, Long> {
}
