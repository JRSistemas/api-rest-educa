package com.jr.sistemas.msv.curso.model;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "CURSO")
public class Curso {
    @Id
    @Column(name = "CUR_ID")
    private Long id;
    @Column(name = "CUR_NOMBRE")
    private String nombre;
    @Column(name = "CUR_VACANTES")
    private Integer vacantes;
    @Column(name = "CUR_MATRICULADOS")
    private Integer matriculados;
    @Column(name = "CUR_PROFESOR")
    private String profesor;
    @Column(name = "CUR_PRECIO")
    private Double precio;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getVacantes() {
        return vacantes;
    }

    public void setVacantes(Integer vacantes) {
        this.vacantes = vacantes;
    }

    public Integer getMatriculados() {
        return matriculados;
    }

    public void setMatriculados(Integer matriculados) {
        this.matriculados = matriculados;
    }

    public String getProfesor() {
        return profesor;
    }

    public void setProfesor(String profesor) {
        this.profesor = profesor;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }
}
