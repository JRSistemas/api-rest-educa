package com.jr.sistemas.msv.curso.controller;


import com.jr.sistemas.msv.curso.model.Curso;
import com.jr.sistemas.msv.curso.service.CursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/curso")
public class CursoController {
    @Autowired
    private CursoService cursoService;

    @GetMapping
    public ResponseEntity<List<Curso>> listar(){
        return ResponseEntity.ok(cursoService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id)
    {
        Optional<Curso> cursoOptional = cursoService.findById(id);
        if(cursoOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(cursoOptional.get());
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping
    public ResponseEntity<?> guardar(@RequestBody Curso curso)
    {
        return ResponseEntity.status(HttpStatus.CREATED).body(cursoService.save(curso));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> editar(@RequestBody Curso curso, @PathVariable Long id){
        Optional<Curso> cursoOptional = cursoService.findById(id);
        if(cursoOptional.isPresent()){
            Curso cursoDb = cursoOptional.get();
            cursoDb.setNombre(curso.getNombre());
            cursoDb.setVacantes(curso.getVacantes());
            cursoDb.setMatriculados(curso.getMatriculados());
            cursoDb.setProfesor(curso.getProfesor());
            cursoDb.setPrecio(curso.getPrecio());
            return ResponseEntity.status(HttpStatus.OK).body(cursoService.save(cursoDb));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> eliminar(@PathVariable Long id){
        Optional<Curso> cursoOptional = cursoService.findById(id);
        if (cursoOptional.isPresent()){
            cursoService.deleteById(id);
            return  ResponseEntity.noContent().build();
        }
        return  ResponseEntity.notFound().build();
    }
}
