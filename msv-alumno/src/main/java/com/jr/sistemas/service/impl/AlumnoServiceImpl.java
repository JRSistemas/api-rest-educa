package com.jr.sistemas.service.impl;

import com.jr.sistemas.model.Alumno;
import com.jr.sistemas.repositories.AlumnoRepository;
import com.jr.sistemas.service.AlumnoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;
@Service
public class AlumnoServiceImpl implements AlumnoService {
    @Autowired
    private AlumnoRepository alumnoRepository;
    @Override
    @Transactional
    public Alumno save(Alumno alumno) {
        return alumnoRepository.save(alumno);
    }
    @Override
    @Transactional(readOnly = true)
    public Optional<Alumno> findById(Long id) {
        return  alumnoRepository.findById(id);
    }
    @Override
    @Transactional(readOnly = true)
    public List<Alumno> findAll() {
        return (List<Alumno>)alumnoRepository.findAll();
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        alumnoRepository.deleteById(id);
    }
}




















