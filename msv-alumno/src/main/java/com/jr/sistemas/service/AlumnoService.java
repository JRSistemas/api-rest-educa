package com.jr.sistemas.service;

import com.jr.sistemas.model.Alumno;
import java.util.List;
import java.util.Optional;

public interface AlumnoService {
    Alumno save(Alumno entity);
    Optional<Alumno> findById(Long id);
    List<Alumno> findAll();
    void deleteById(Long id);
}
