package com.jr.sistemas.repositories;

import com.jr.sistemas.model.Alumno;
import org.springframework.data.repository.CrudRepository;

public interface AlumnoRepository extends CrudRepository<Alumno, Long> {
}
