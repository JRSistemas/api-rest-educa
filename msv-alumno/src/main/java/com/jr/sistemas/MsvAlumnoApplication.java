package com.jr.sistemas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsvAlumnoApplication {
 public static void main(String[] args) {
		SpringApplication.run(MsvAlumnoApplication.class, args);
	}

}
