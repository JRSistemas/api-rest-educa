package com.jr.sistemas.controller;

import com.jr.sistemas.model.Alumno;
import com.jr.sistemas.service.AlumnoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/alumno")
public class AlumnoController {
    @Autowired
    private AlumnoService alumnoService;

    // Endpoint que lista los Alumnos
    @GetMapping
    public ResponseEntity<List<Alumno>> listar(){
        return ResponseEntity.status(HttpStatus.OK).body(alumnoService.findAll());
    }

    // Enpoint que hace la busqueda de un Alumno
    @GetMapping("/{id}")
    public ResponseEntity<?> porId(@PathVariable Long id){
        Optional<Alumno> aluOptional = alumnoService.findById(id);
        if(aluOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(aluOptional.get());
        }
        return ResponseEntity.notFound().build();
    }

    // Enpoint para agregar un Alumno
    @PostMapping
    public ResponseEntity<?> guardar(@RequestBody Alumno alumno){
        return ResponseEntity.status(HttpStatus.CREATED).body(alumnoService.save(alumno));
    }

    // Endpoint para actualizar un Alumno
    @PutMapping("/{id}")
    public ResponseEntity<?> editar(@RequestBody Alumno alumno, @PathVariable Long id){
        Optional<Alumno> aluOptional = alumnoService.findById(id);
        if(aluOptional.isPresent()){
            Alumno alumnoDb = aluOptional.get();
            alumnoDb.setNombre(alumno.getNombre());
            alumnoDb.setDireccion(alumno.getDireccion());
            alumnoDb.setTelefono(alumno.getTelefono());
            alumnoDb.setEmail(alumno.getEmail());
            return ResponseEntity.status(HttpStatus.OK).body(alumnoService.save(alumnoDb));
        }
        return ResponseEntity.notFound().build();
    }

    // Endpoint para eliminar un Alumno
    @DeleteMapping("/{id}")
    public ResponseEntity<?> eliminar(@PathVariable Long id)
    {
        Optional<Alumno> aluOptional = alumnoService.findById(id);
        if (aluOptional.isPresent()){
            alumnoService.deleteById(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        return  ResponseEntity.notFound().build();
    }
}




































