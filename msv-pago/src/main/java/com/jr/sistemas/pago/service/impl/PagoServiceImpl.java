package com.jr.sistemas.pago.service.impl;

import com.jr.sistemas.pago.model.Pago;
import com.jr.sistemas.pago.model.PagoPK;
import com.jr.sistemas.pago.repositories.PagoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;
@Service
public class PagoServiceImpl implements PagoService {
    @Autowired
    private PagoRepository pagoRepository;
    @Override
    @Transactional
    public Pago save(Pago pago) {
        return pagoRepository.save(pago);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Pago> findById(PagoPK id) {
        return pagoRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Pago> findAll() {
        return (List<Pago>)pagoRepository.findAll();
    }
    @Override
    @Transactional
    public void deleteById(PagoPK id) {
        pagoRepository.deleteById(id);
    }
}
