package com.jr.sistemas.pago;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsvPagoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsvPagoApplication.class, args);
	}

}
