package com.jr.sistemas.pago.model;

import jakarta.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "PAGO", schema = "EDUCA")
public class Pago implements Serializable {

    @EmbeddedId
    private PagoPK id;
    @Column(name = "PAG_FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime fecha;
    @Column(name = "PAG_IMPORTE")
    private Double importe;

    public PagoPK getId() {
        return id;
    }

    public void setId(PagoPK id) {
        this.id = id;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public Double getImporte() {
        return importe;
    }

    public void setImporte(Double importe) {
        this.importe = importe;
    }
}
