package com.jr.sistemas.pago.service.impl;

import com.jr.sistemas.pago.model.Pago;
import com.jr.sistemas.pago.model.PagoPK;
import java.util.List;
import java.util.Optional;

public interface PagoService {
    Pago save(Pago pago);
    Optional<Pago> findById(PagoPK id);
    List<Pago> findAll();
    void deleteById(PagoPK id);

}
