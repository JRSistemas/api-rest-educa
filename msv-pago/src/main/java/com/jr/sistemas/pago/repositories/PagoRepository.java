package com.jr.sistemas.pago.repositories;

import com.jr.sistemas.pago.model.Pago;
import com.jr.sistemas.pago.model.PagoPK;
import org.springframework.data.repository.CrudRepository;

public interface PagoRepository extends CrudRepository<Pago, PagoPK> {
}
