package com.jr.sistemas.pago.controller;

import com.jr.sistemas.pago.model.Pago;
import com.jr.sistemas.pago.model.PagoPK;
import com.jr.sistemas.pago.service.impl.PagoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/pago")
public class PagoController {
    @Autowired
    private PagoService pagoService;
    @GetMapping
    public ResponseEntity<List<Pago>> listar(){
            return ResponseEntity.ok().body(pagoService.findAll());
    }

    @GetMapping("/{curId}/{aluId}/{pagCuota}")
    public ResponseEntity<?> porId(@PathVariable Long curId, @PathVariable Long aluId, @PathVariable Long pagCuota){
        Pago pago = new Pago();
        PagoPK pagoPk = new PagoPK();
        pagoPk.setCurId(curId);
        pagoPk.setAluId(aluId);
        pagoPk.setPagCuota(pagCuota);
        pago.setId(pagoPk);

        Optional<Pago> pagoOptional = pagoService.findById(pago.getId());
        if(pagoOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(pagoOptional.get());
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping
    public ResponseEntity<?> guardar(@RequestBody Pago pago){
        return ResponseEntity.status(HttpStatus.CREATED).body(pagoService.save(pago));
    }

    @PutMapping("/{curId}/{aluId}/{pagCuota}")
    public ResponseEntity<?> editar(@PathVariable Long curId, @PathVariable Long aluId, @PathVariable Long pagCuota, @RequestBody Pago pago){
        Pago pagoDb = new Pago();
        PagoPK pagoPK = new PagoPK();
        pagoPK.setCurId(curId);
        pagoPK.setAluId(aluId);
        pagoPK.setPagCuota(pagCuota);
        pagoDb.setId(pagoPK);

        Optional<Pago> pagoOptional = pagoService.findById(pagoDb.getId());
        if (pagoOptional.isPresent()){
            pagoDb.setFecha(pago.getFecha());
            pagoDb.setImporte(pago.getImporte());
            return  ResponseEntity.status(HttpStatus.OK).body(pagoService.save(pagoDb));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{curId}/{aluId}/{pagCuota}")
    public ResponseEntity<?> eliminar(@PathVariable Long curId, @PathVariable Long aluId, @PathVariable Long pagCuota){
        Pago pago = new Pago();
        PagoPK pagoPK = new PagoPK();
        pagoPK.setCurId(curId);
        pagoPK.setAluId(aluId);
        pagoPK.setPagCuota(pagCuota);
        pago.setId(pagoPK);

        Optional<Pago> pagoOptional = pagoService.findById(pago.getId());
        if(pagoOptional.isPresent()){
            pagoService.deleteById(pago.getId());
            return  ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        return ResponseEntity.notFound().build();
    }
}





