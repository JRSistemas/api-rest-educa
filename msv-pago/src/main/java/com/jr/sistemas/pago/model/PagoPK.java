package com.jr.sistemas.pago.model;

import jakarta.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class PagoPK  implements Serializable {
    private Long curId;
    private Long aluId;
    private Long pagCuota;

    public Long getCurId() {
        return curId;
    }

    public void setCurId(Long curId) {
        this.curId = curId;
    }

    public Long getAluId() {
        return aluId;
    }

    public void setAluId(Long aluId) {
        this.aluId = aluId;
    }

    public Long getPagCuota() {
        return pagCuota;
    }

    public void setPagCuota(Long pagCuota) {
        this.pagCuota = pagCuota;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PagoPK pagoPK = (PagoPK) o;

        if (!Objects.equals(curId, pagoPK.curId)) return false;
        if (!Objects.equals(aluId, pagoPK.aluId)) return false;
        return Objects.equals(pagCuota, pagoPK.pagCuota);
    }

    @Override
    public int hashCode() {
        int result = curId != null ? curId.hashCode() : 0;
        result = 31 * result + (aluId != null ? aluId.hashCode() : 0);
        result = 31 * result + (pagCuota != null ? pagCuota.hashCode() : 0);
        return result;
    }
}
