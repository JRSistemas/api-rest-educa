package com.jr.sistemas.matricula.model;

import jakarta.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class MatriculaPk implements Serializable {
    private Long curId;
    private Long aluId;

    public Long getCurId() {
        return curId;
    }

    public void setCurId(Long curId) {
        this.curId = curId;
    }

    public Long getAluId() {
        return aluId;
    }

    public void setAluId(Long aluId) {
        this.aluId = aluId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MatriculaPk that = (MatriculaPk) o;

        if (!Objects.equals(curId, that.curId)) return false;
        return Objects.equals(aluId, that.aluId);
    }

    @Override
    public int hashCode() {
        int result = curId != null ? curId.hashCode() : 0;
        result = 31 * result + (aluId != null ? aluId.hashCode() : 0);
        return result;
    }
}






























