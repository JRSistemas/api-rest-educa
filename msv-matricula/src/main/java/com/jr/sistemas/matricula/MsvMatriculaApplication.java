package com.jr.sistemas.matricula;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsvMatriculaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsvMatriculaApplication.class, args);
	}

}
