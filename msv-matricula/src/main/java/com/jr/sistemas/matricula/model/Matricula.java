package com.jr.sistemas.matricula.model;

import jakarta.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "MATRICULA", schema = "EDUCA")
public class Matricula implements Serializable {

    @EmbeddedId
    private MatriculaPk id;
    @Column(name = "MAT_FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime fecha;
    @Column(name = "MAT_PRECIO")
    private Double precio;

    @Column(name = "MAT_CUOTAS")
    private Integer cuota;

    @Column(name = "MAT_NOTA")
    private Double nota;

    public MatriculaPk getId() {
        return id;
    }

    public void setId(MatriculaPk id) {
        this.id = id;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Integer getCuota() {
        return cuota;
    }

    public void setCuota(Integer cuota) {
        this.cuota = cuota;
    }

    public Double getNota() {
        return nota;
    }

    public void setNota(Double nota) {
        this.nota = nota;
    }
}
